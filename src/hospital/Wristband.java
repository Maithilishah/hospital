/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital;

/**
 *
 * @author Admin
 */
public class Wristband {
    
    String name;
    Doctor doc;
    int dob;

    public Wristband(String name, Doctor doc, int dob) {
        this.name = name;
        this.doc = doc;
        this.dob = dob;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Doctor getDoc() {
        return doc;
    }

    public void setDoc(Doctor doc) {
        this.doc = doc;
    }

    public int getDob() {
        return dob;
    }

    public void setDob(int dob) {
        this.dob = dob;
    }

    @Override
    public String toString() {
        return "Wristband{" + "name=" + name + ", doc=" + doc + ", dob=" + dob + '}';
    }
    
    
}
